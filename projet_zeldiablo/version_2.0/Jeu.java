import java.util.ArrayList;

public class Jeu {
    /**
     * Plateau de jeu
     */
    private Plateau p;
    Hero moi;
    ArrayList<Monstre> lm;

    /**
     * Constructeur vide de Jeu
     */
    public Jeu() { }

    /**
     * Méthode qui initialise un pLateau dans le jeu
     */
    public void initialiserPlateau(){
        this.p = new Plateau();
    }

    /**
     * Methode permettant d'initialiser le Hero du jeu
     * @param nom le nom du Hero
     */
    public void initialiserhero(String nom) {
        this.moi = new Hero(nom);
    }

    /**
     * Methode permettant d'initialiser la liste de monstres de jeu
     * @param nbMonstre Le nombre de monstres dans la liste
     */
    public void initialiserMonstre(int nbMonstre) {
        this.lm = new ArrayList<>();
        for (int i = 0; i < nbMonstre; i++){
            this.lm.add(new Monstre());
        }
    }

    /**
     * Methodes Getters
     */
    public Plateau getPlateau(){
        return this.p;
    }
    public Hero getHero(){
        return this.moi;
    }
    public Monstre getMonstre(int index){
        return this.lm.get(index);
    }
    public int getNbMonstre(){
        return this.lm.size();
    }
}
