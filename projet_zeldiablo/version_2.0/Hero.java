public class Hero extends Personnage{

  /**
   * le nom du Hero
   */
  private String nom;


  /**
   * Constructeur vide de Hero
   */
  public Hero(){
    super(5,2,1);
    this.nom="Timmie";
  }

  /**
   * Constructeur ne prenant uniquement qu'un String (le nom)
   * @param String n : le nom du hero
   */
  public Hero(String n){
    super(5,2,1);
    if(!n.equals("")){
      this.nom=n;
    } else {
      this.nom="Timmie";
    }
  }

  /**
   * Constructeur initialisent tous les attributs de Hero
   * @param int pv : le nombre de point de vie du Hero
   * @param int dgt : les degat qu'inflige le Hero
   * @param int port : la portée d'attaque du Hero
   * @param String n : le nom du hero
   */
  public Hero(int pv, int dgt, int port, String n){
    super(pv,dgt,port);
    if(!n.equals("")){
      this.nom=n;
    } else {
      this.nom="Timmie";
    }
  }


  /**
   * @return String getNom retourne le nom du héro
   */
  public String getNom(){
    return this.nom;
  }
}
