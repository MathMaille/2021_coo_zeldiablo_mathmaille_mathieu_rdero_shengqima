public abstract class Personnage{

  /**
   * Attributs du Personnage
   */
  //vie du personnage
  private int vie;
  //dégats du personnage
  private int degat;
  //portée d'attaque du personnage
  private int portee;
  //position du personnage en x
  private int x;
  //position du personnage en y
  private int y;

  /**
   *
   * Constructeur de Personnage
   *
   * @param pv   int   points de vie du personnage
   * @param dgt   int   dégats du personnage
   * @param port  int  portée d'attaque du personnage
   */
  public Personnage(int pv,int dgt, int port){
    if(pv > 0 && dgt > 0 && port > 0){
      this.vie=pv;
      this.degat=dgt;
      this.portee=port;
    }
  }

  /**
   * Getter vie
   *
   * @return int   vie du personnage
   */
  public int getVie(){
    return this.vie;
  }

  /**
   * Getter degat
   *
   * @return int  degats du personnage
   */
  public int getDegat(){
    return this.degat;
  }

  /**
   * Getter Portee
   *
   * @return int   portée du personnage
   */
  public int getPortee(){
    return this.portee;
  }
}
