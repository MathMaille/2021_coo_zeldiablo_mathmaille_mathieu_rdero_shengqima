public interface Case {
    /**
     * Méthode qui permet de déterminer si la case est un mur
     *
     * @return boolean  true si la case est un mur
     */
    public boolean etreMur();
}
