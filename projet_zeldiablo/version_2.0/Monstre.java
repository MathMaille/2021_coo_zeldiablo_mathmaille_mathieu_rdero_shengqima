public class Monstre extends Personnage{

      /**
       * Constructeur vide de Monstre
       */
      public Monstre(){
        super(2,1,1);
      }

      /**
       * Constructeur initialisent tous les attributs de Monstre
       * @param int pv : le nombre de point de vie du Hero
       * @param int dgt : les degat qu'inflige le Hero
       * @param int port : la portée d'attaque du Hero
       */
      public Monstre(int pv, int dgt, int port){
        super(pv,dgt,port);
      }

}
