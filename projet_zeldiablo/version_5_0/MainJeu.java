package version_5_0;

import version_5_0.JeuPackage.*;
import version_5_0.moteurJeu.MoteurGraphique;

import java.util.Scanner;

public class MainJeu {

    public static void main(String[] args) throws InterruptedException {
        Game leJeu = new Game("Timy");
        DessinGame interfaceJeu = new DessinGame(leJeu);

        // classe qui lance le moteur de jeu generique
        MoteurGraphique moteur = new MoteurGraphique(leJeu, interfaceJeu);
        // lance la boucle de jeu qui tourne jusque la fin du jeu
        moteur.lancerJeu(500, 525);

        // lorsque le jeu est fini
        System.out.println("Fin du Jeu - appuyer sur entree");
        Scanner sc = new Scanner(System.in);
        sc.nextLine();
        System.exit(1);

    }
}
