package version_5_0.JeuPackage;

public class Mur implements Case {

    /**
     * Constructeur de Mur
     */
    public Mur() { }

    /**
     *
     * @return boolean   true car c'est un mur
     */
    @Override
    public boolean etreMur() {
        return true;
    }

    /**
     * Determine si la case possede un personnage
     * @return boolean true si la case possede un personnage
     */
    @Override
    public boolean etreOccupe() {
        return false;
    }

    /**
     * C'est un mur, certains monstres peuvent les traverser
     * @param p le personnage
     */
    @Override
    public void setPersonnage(Personnage p) {

    }

    /**
     * Retire le personnage de la case
     */
    @Override
    public void removePersonnage() {

    }

    /**
     * @return personnage qui occupe la case
     */
    @Override
    public Personnage getPersonnage() {
        return null;
    }

    /**
     *
     * @param t  Piege à placer sur la case
     */
    @Override
    public void setTrap(Piege t){

    }

    /**
     * enlève le piège de la case
     */
    @Override
    public void removeTrap(){

    }


    /**
     *
     * @return boolean  false
     */
    @Override
    public boolean etrePiege(){
        return false;
    }

    /**
     *
     * @return  null   la case est un mur
     */
    @Override
    public Piege getTrap(){
        return null;
    }

    @Override
    public boolean etreSortie() {
        return false;
    }

    @Override
    public void setSortie() {

    }
}
