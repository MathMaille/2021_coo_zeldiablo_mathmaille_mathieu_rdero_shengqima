package version_5_0.JeuPackage;

import version_5_0.moteurJeu.Commande;
import version_5_0.moteurJeu.Jeu;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.ArrayList;

public class Game implements Jeu {

    //Le plateau du jeu
    private Plateau p;
    //Notre personnage jouable
    private final Hero moi;
    //Les monstres
    private ArrayList<Monstre> lm;
    //Le timer des attques des monstres
    private int timer=0;
    //Le boolean de victoire
    private boolean win = false;
    private boolean winDessin = false;

    /**
     * Constructeur de la classe jeu creant un plateau pre defini
     * @param nom String nom du hero
     */
    public Game(String nom){
        this.moi = new Hero(nom);
        try {
            lireFichier("lvl1.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    /**
     * Methode permettant de lire un fichier pour creer un niveau
     * @param nom String: Le nom du niveau a creer
     * @throws IOException: Erreur de lancement
     */
    public void lireFichier(String nom) throws IOException {

        lm = new ArrayList<>();
        BufferedReader br = new BufferedReader(new FileReader("Data/"+nom));
        String st;
        int j =0;
        Monstre m;
        st = br.readLine();
        this.p = new Plateau(st.length(),st.length());
        do {
            for (int i = 0; i < st.length(); i++) {
                char a = st.charAt(i);
                switch (a) {
                    case '#' -> this.p.placerMur(i, j);
                    case 'M' -> {
                        m = new Monstre();
                        m.setPlace(i, j);
                        this.lm.add(m);
                        this.p.setPersonnage(m, i, j);
                    }
                    case 'J' -> {
                        this.p.setPersonnage(moi, i, j);
                        moi.setPlace(i,j);
                    }
                    case 'P' -> {
                        this.p.getCase(i,j).setTrap(new Piege());
                    }
                    case 'S' -> {
                        this.p.getCase(i,j).setSortie();
                    }
                }
            }
            j++;
        } while ((st = br.readLine()) != null);
    }


    /**
     * Methode initialisant un plateau avec des murs aleatoires.
     */
    public void initiliserPlateau(){
        this.p = new Plateau();
        for(int i=0;i<10;i++){
            boolean trouver = false;
            while(!trouver){
                int k = (int) (Math.random() * (10));
                int j = (int) (Math.random() * (10));
                if(!this.p.getCase(j,k).etreMur()){
                    this.p.placerMur(j,k);
                    trouver = true;
                }
            }
        }

    }


    /**
     * Methodes Getters
     */
    public Plateau getPlateau(){
        return this.p;
    }
    public Hero getHero(){
        return this.moi;
    }
    public Monstre getMonstre(int index){
        return this.lm.get(index);
    }
    public int getNbMonstre(){
        return this.lm.size();
    }
    public ArrayList<Monstre> getLm(){
        return lm;
    }

    /**
     * Si le Hero meurt le jeu est défaite
     * @return true quand le jeu est défaite false sinon
     * Attention : le jeu n'est pas défaite != le jeu est victoire
     */
    public boolean etreDefaite(){
        return moi.getMort();
    }

    /**
     * Si tous les monstres meurent et le hero vivant
     * @return true si le jeu est victoire
     */
    public boolean etreVictoire(){
        return lm.size() == 0 && !etreDefaite();
    }

    /**
     * Methode qui permet à un personnage d'en attaquer un autre
     * @param p le personnage qui essaye d'attaquer;
     */
    public void attaquerPerso(Personnage p, Commande commandeUser) {

        if (commandeUser.action && !moi.getMort()){
        int i = 1;
        //Verfifie chaque cases dans la portee du hero
        while (i <= p.getPortee()) {
            //Et verifie les case dans les directions du hero
            switch (p.getSens()) {
                case "haut" -> {
                    if (p.getY() - i < 0) break;
                    //Si un mur sépare le monstre et le hero, l'attaque n'a pas lieu
                    if (getPlateau().getCase(p.getX(), p.getY() - i).etreMur()) {
                        i = p.getPortee() + 1;
                    }
                    //Si une case est occupe par un personnage, l'attaque a lieu
                    else if (getPlateau().getCase(p.getX(), p.getY() - i).etreOccupe()) {
                        getPlateau().getCase(p.getX(), p.getY() - i).getPersonnage().subirDegat(p.getDegat());
                        i = p.getPortee() + 1;
                    }
                }
                case "bas" -> {
                    if (p.getY() + i > this.p.getTailleY() - 1) break;
                    if (getPlateau().getCase(p.getX(), p.getY() + i).etreMur()) {
                        i = p.getPortee();
                    } else if (getPlateau().getCase(p.getX(), p.getY() + i).etreOccupe()) {
                        getPlateau().getCase(p.getX(), p.getY() + i).getPersonnage().subirDegat(p.getDegat());
                        i = p.getPortee();
                    }
                }
                case "droite" -> {
                    if (p.getX() + i > this.p.getTailleX() - 1) break;
                    if (getPlateau().getCase(p.getX() + i, p.getY()).etreMur()) {
                        i = p.getPortee();
                    } else if (getPlateau().getCase(p.getX() + i, p.getY()).etreOccupe()) {
                        getPlateau().getCase(p.getX() + i, p.getY()).getPersonnage().subirDegat(p.getDegat());
                        i = p.getPortee();
                    }
                }
                case "gauche" -> {
                    if (p.getX() - i < 0) break;
                    if (getPlateau().getCase(p.getX() - i, p.getY()).etreMur()) {
                        i = p.getPortee();
                    } else if (getPlateau().getCase(p.getX() - i, p.getY()).etreOccupe()) {
                        getPlateau().getCase(p.getX() - i, p.getY()).getPersonnage().subirDegat(p.getDegat());
                        i = p.getPortee();
                    }
                }
            }
            i++;
        }
        //Retire les monstres morts
        for (Monstre monstre : lm) {
            if (monstre.getMort()) {
                this.p.getCase(monstre.getX(), monstre.getY()).removePersonnage();
                this.lm.remove(monstre);
                break;
            }
        }
    }
    }

    /**
     * Methode deplacant le hero selon la commande de l'utilisateur
     * @param commandeUser la classe contenant la direction choisi par l'utilisateur
     */
    public void deplacerHero(Commande commandeUser) {
        if (!moi.getMort()) {
            //Retire le personnage de la case qu'il occupe
            this.p.getCase(moi.getX(), moi.getY()).removePersonnage();
            //Verifie la commande effectue et que le hero ne sorte pas du plateau
            if (commandeUser.bas && !commandeUser.haut && !commandeUser.gauche && !commandeUser.droite && moi.getY() < 9) {
                moi.setSens("bas");
                //get la case et verifie si elle est libre et pas un mur et effectue le deplacement
                if (!getPlateau().getCase(moi.getX(), moi.getY() + 1).etreMur() &&
                        !getPlateau().getCase(moi.getX(), moi.getY() + 1).etreOccupe()) {
                    moi.deplacer(commandeUser);
                    prendrePiege(moi);
                }
            } else if (commandeUser.droite && !commandeUser.bas && !commandeUser.haut && !commandeUser.gauche && moi.getX() < 9) {
                moi.setSens("droite");
                if (!getPlateau().getCase(moi.getX() + 1, moi.getY()).etreMur() &&
                        !getPlateau().getCase(moi.getX() + 1, moi.getY()).etreOccupe()) {
                    moi.deplacer(commandeUser);
                    prendrePiege(moi);
                }
            } else if (commandeUser.haut && !commandeUser.bas && !commandeUser.gauche && !commandeUser.droite && moi.getY() > 0) {
                moi.setSens("haut");
                if (!getPlateau().getCase(moi.getX(), moi.getY() - 1).etreMur() &&
                        !getPlateau().getCase(moi.getX(), moi.getY() - 1).etreOccupe()) {
                    moi.deplacer(commandeUser);
                    prendrePiege(moi);
                }
            } else if (commandeUser.gauche && !commandeUser.bas && !commandeUser.haut && !commandeUser.droite && moi.getX() > 0) {
                moi.setSens("gauche");
                if (!getPlateau().getCase(moi.getX() - 1, moi.getY()).etreMur() &&
                        !getPlateau().getCase(moi.getX() - 1, moi.getY()).etreOccupe()) {
                    moi.deplacer(commandeUser);
                    prendrePiege(moi);
                }
            }
            //Remet le personnage dans la case avec ses nouvelles coordonnees.
            this.p.getCase(moi.getX(), moi.getY()).setPersonnage(moi);
            if(this.p.getCase(moi.getX(), moi.getY()).etreSortie()){
                moi.monterEtage();
                sortie(moi.getNEtage());
            }
        }
    }

    /**
     *
     * @param pa Personnage   personnage qui se prend le piège
     */
    public void prendrePiege(Personnage pa){
        Case ca = this.p.getCase(pa.getX(), pa.getY());
        //vérifier piege
        if (ca.etrePiege()){
            //si piege, blesser
            ca.getTrap().Blesser(pa);
            //supprimer piege
            this.p.getCase(pa.getX(), pa.getY()).removeTrap();
        }
    }

    /**
     * Methode s'occupant de l'attaque des monstres
     * @param m Monstre: le monstre qui attaque
     */
    public void attaquerMonstre(Monstre m){
        int i;
        if(!this.moi.getMort()){
            i =1;
            while ( i<=m.getPortee()){
                if(m.getY() - i >= 0){
                    if (getPlateau().getCase(m.getX(), m.getY() - i).etreOccupe()) {
                        getPlateau().getCase(m.getX(), m.getY() - i).getPersonnage().subirDegat(m.getDegat());
                    }
                }
                if(m.getY() + i < 10){
                    if (getPlateau().getCase(m.getX(), m.getY() + i).etreOccupe()) {
                        getPlateau().getCase(m.getX(), m.getY() + i).getPersonnage().subirDegat(m.getDegat());
                    }
                }
                if(m.getX() + i < 10){
                    if (getPlateau().getCase(m.getX()+i, m.getY()).etreOccupe()) {
                        getPlateau().getCase(m.getX()+i, m.getY()).getPersonnage().subirDegat(m.getDegat());
                    }
                }
                if(m.getX() - i >= 0){
                    if (getPlateau().getCase(m.getX()-i, m.getY()).etreOccupe()) {
                        getPlateau().getCase(m.getX()-i, m.getY()).getPersonnage().subirDegat(m.getDegat());
                    }
                }
                i++;
            }
        }
    }

    /**
     * Methode s'occupant du deplacement des monstres
     * @param m Monstre: le monstre qui se deplace
     */
    public void seDeplacer(Monstre m){
        this.p.getCase(m.getX(),m.getY()).removePersonnage();
        int k = (int) Math.round(Math.random()*(3));
        switch(k){
            case 0:
                if (m.getY()-1 >= 0) {
                    if (!getPlateau().getCase(m.getX(), m.getY() - 1).etreMur() &&
                            !getPlateau().getCase(m.getX(),m.getY()-1).etreOccupe() ){
                        m.setPlace(m.getX(),m.getY()-1);
                    }
                }
                break;
            case 1:
                if (m.getY()+1 < this.p.getTailleY()){
                    if (!getPlateau().getCase(m.getX(), m.getY() + 1).etreMur() &&
                            !getPlateau().getCase(m.getX(),m.getY()+1).etreOccupe()){
                        m.setPlace(m.getX(),m.getY()+1);
                    }
                }
                break;
            case 2:
                if (m.getX()-1 >= 0){
                    if (!getPlateau().getCase(m.getX()-1, m.getY()).etreMur()  &&
                            !getPlateau().getCase(m.getX()-1,m.getY()).etreOccupe()){
                        m.setPlace(m.getX()-1,m.getY());
                    }
                }
                break;
            case 3:
                if (m.getX()+1<this.p.getTailleX()){
                    if (!getPlateau().getCase(m.getX()+1, m.getY()).etreMur() &&
                            !getPlateau().getCase(m.getX()+1,m.getY()).etreOccupe()){
                        m.setPlace(m.getX()+1,m.getY());
                    }
                }
                break;
        }
        this.p.getCase(m.getX(), m.getY()).setPersonnage(m);
    }

    /**
     * Methode qui gere les monstres
     */
    public void monstresAutonomes(){
        for (Monstre monstre : lm) {
            if ((Math.abs(monstre.getX() - this.moi.getX()) <= monstre.getPortee()) && (monstre.getY() == this.moi.getY()) || (Math.abs(monstre.getY() - this.moi.getY()) <= monstre.getPortee() && (monstre.getX() == this.moi.getX()))) {
                attaquerMonstre(monstre);
            } else {
                seDeplacer(monstre);
            }
        }
    }

    /**
     * Methode du moteur du jeu s'effectuant plusieurs fois par secondes
     * @param commandeUser la classe des commandes que l'utilisateur peut utiliser
     */
    @Override
    public void evoluer(Commande commandeUser) {
        etreDefaite();
        //A chaque appelle deplace le personnage selon les commandes de l'utilisateur
        deplacerHero(commandeUser);
        //A chaque appelle attaque avec le personnage si l'utilisateur utilise la bonne commande
        attaquerPerso(moi,commandeUser);
        this.timer++;
        if (this.timer == 5){
            monstresAutonomes();
            this.timer = 0;
        }
    }

    /**
     * Arrete le jeu si retourne false
     * @return false quand le jeu est fini.
     */
    @Override
    public boolean etreFini() {
        return win;
    }

    /**
     * Methode se declanchant en etant sur une sortie et parmet de passer au niveau suivant
     * @param n int: l'etage actuel
     */
    public void sortie(int n){
        if(n>=5){
            winDessin = true;
        }else{
            String nomFich = "lvl"+n+".txt";
            try {
                lireFichier(nomFich);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @return true pour afficher le dessin de victoire
     */
    public boolean getWinDessin(){
        return winDessin;
    }
    public void setWin(){
        this.win = true;
    }


}
