package version_5_0.JeuPackage;

public class CaseLibre implements Case {

    Personnage occupe;
    private Piege trap;
    private boolean sortie=false;

    /**
     * Constructeur vide de CaseLibre
     */
    public CaseLibre(){ }

    /**
     * @return boolean   false car ce n'est pas un mur
     */
    @Override
    public boolean etreMur() {
        return false;
    }

    /**
     * Placer une personnage pour un caseLibre
     */
    @Override
    public void setPersonnage(Personnage p) {
        if( !etreOccupe() )
            this.occupe = p;
    }
    /**
     * Determine si la case possede un personnage
     * @return boolean true si la case possede un personnage
     */
    @Override
    public boolean etreOccupe() {
        return this.occupe != null;
    }
    /**
     * Retire le personnage de la case
     */
    public void removePersonnage() {
        if(etreOccupe()) {
            this.occupe = null;
        }
    }
    /**
     * @return personnage qui occupe la case
     */
    public Personnage getPersonnage() {
        return this.occupe;
    }
    /**
     *
     * @param t  Piege à placer sur la case
     */
    @Override
    public void setTrap(Piege t){
        this.trap = t;
    }

    /**
     * enlève le piège de la case
     */
    @Override
    public void removeTrap(){
        this.trap = null;
    }


    /**
     * @return boolean  true si la case est piégée
     */
    @Override
    public boolean etrePiege(){
        return this.trap != null;
    }

    /**
     *
     * @return  Piege   le piege qui est sur la cases
     */
    public Piege getTrap(){
        return this.trap;
    }

    @Override
    public boolean etreSortie() {
        return this.sortie;
    }

    @Override
    public void setSortie(){
        this.sortie=true;
    }

}
