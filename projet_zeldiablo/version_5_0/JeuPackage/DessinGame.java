package version_5_0.JeuPackage;

import version_5_0.moteurJeu.*;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class DessinGame implements DessinJeu {

    //Le jeu est en attribut pour que la classe ai acces aux infos du jeu
    private final Game modele;
    //Defini la taille des des cases du plateau
    private final int tailleCase=50;

    /**
     * Constructeur de la classe
     * @param g le jeu a afficher
     */
    public DessinGame(Game g){
        this.modele = g;

    }

    /**
     * Methode permettant de dessiner l'interface du jeu
     * @param image image contenant le dessin
     */
    @Override
    public void dessiner(BufferedImage image) {
        Graphics2D crayon = (Graphics2D) image.getGraphics();
        BufferedImage img ;
        for(int i =0; i < modele.getPlateau().getTailleX(); i++){
            for(int j = 0; j< modele.getPlateau().getTailleY();j++){
                img =null;
                try {
                    if (modele.getPlateau().getCase(i, j).etrePiege()){
                        img = ImageIO.read(new File("Data/piege.png"));
                    } else if (modele.getPlateau().getCase(i, j).etreMur()) {
                        img = ImageIO.read(new File("Data/mur.png"));
                    } else if(modele.getPlateau().getCase(i, j).etreSortie()) {
                        img = ImageIO.read(new File("Data/sortie.png"));
                    }else {
                        img = ImageIO.read(new File("Data/grass1.png"));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                crayon.drawImage(img, i*tailleCase,j*tailleCase,null);
            }
        }
        img = null;
        try {
            img = switch (modele.getHero().getSens()) {
                case "bas" -> ImageIO.read(new File("Data/bas.png"));
                case "haut" -> ImageIO.read(new File("Data/haut.png"));
                case "droite" -> ImageIO.read(new File("Data/droite.png"));
                case "gauche" -> ImageIO.read(new File("Data/gauche.png"));
                default -> img;
            };
            if (modele.getHero().getMort()){
                img = ImageIO.read(new File("Data/mort.png"));
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
        crayon.drawImage(img, modele.getHero().getX()*tailleCase,modele.getHero().getY()*tailleCase,null);
        try {
            img = ImageIO.read(new File("Data/m.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        for(int i=0; i < modele.getNbMonstre(); i++){
            crayon.drawImage(img, modele.getMonstre(i).getX()*tailleCase,modele.getMonstre(i).getY()*tailleCase,null);
        }
        crayon.setColor(Color.BLACK);
        crayon.fillRect(0,500,500,25);
        crayon.setColor(Color.GREEN);
        crayon.fillRect(0,500,modele.getHero().getVie()*100, 25);

        if(modele.etreDefaite()){
            try {
                img = ImageIO.read(new File("Data/lost.png"));
            } catch (IOException e) {
                e.printStackTrace();
            }
            crayon.drawImage(img, -50,-50,null);
        }
        else if(modele.getWinDessin()){
            try {
                img = ImageIO.read(new File("Data/vic.png"));
            } catch (IOException e) {
                e.printStackTrace();
            }
            crayon.drawImage(img, 0,100,500,300,null);
            modele.setWin();
        }

    }
}
