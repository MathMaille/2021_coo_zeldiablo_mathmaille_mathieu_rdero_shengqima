package version_5_0.Tests;

import static org.junit.Assert.*;
import org.junit.Test;
import version_5_0.JeuPackage.Hero;

/**
 * classe qui permet de tester le Hero
 */
public class TestHero {
    /**
     * verifie les attributs du héro avec le contructeur vide
     */
    @Test
    public void testHeroVide() {
        Hero h=new Hero();
        assertEquals("il devrait  s'appeler Timmie","Timmie",h.getNom());
        assertEquals("il devrait  avoir 5 de portée",5,h.getVie());
        assertEquals("il devrait  avoir 2 de portée",2,h.getDegat());
        assertEquals("il devrait  avoir 1 de portée",1,h.getPortee());
    }

    /**
     * verifie les attributs du héro avec le contructeur vide
     */
    @Test
    public void testHeroNom() {
        Hero h=new Hero("Tom");
        assertEquals("il devrait  s'appeler Tom","Tom",h.getNom());
        assertEquals("il devrait  avoir 5 de portée",5,h.getVie());
        assertEquals("il devrait  avoir 2 de portée",2,h.getDegat());
        assertEquals("il devrait  avoir 1 de portée",1,h.getPortee());
    }

    /**
     * verifie les attributs du héro avec le contructeur
     */
    @Test
    public void testHero() {
        Hero h=new Hero(10, 5, 2, "Alder");
        assertEquals("il devrait  s'appeler Alder","Alder",h.getNom());
        assertEquals("il devrait  avoir 5 de portée",10,h.getVie());
        assertEquals("il devrait  avoir 2 de portée",5,h.getDegat());
        assertEquals("il devrait  avoir 1 de portée",2,h.getPortee());
    }

    /**
     * verifie les attributs du héro avec le contructeur nom vide
     */
    @Test
    public void testHeroNomVide() {
        Hero h=new Hero(10, 5, 2, "");
        Hero l=new Hero("");
        assertEquals("il devrait  s'appeler Timmie","Timmie",h.getNom());
        assertEquals("il devrait  s'appeler Timmie","Timmie",l.getNom());
        assertEquals("il devrait  avoir 5 de portée",10,h.getVie());
        assertEquals("il devrait  avoir 2 de portée",5,h.getDegat());
        assertEquals("il devrait  avoir 1 de portée",2,h.getPortee());
    }
}