package version_5_0.Tests;

import static org.junit.Assert.*;
import org.junit.Test;
import version_5_0.JeuPackage.Game;
import version_5_0.moteurJeu.Commande;

public class TestJeu {

    @Test
    public void testdeplacer(){
        Game j = new Game("roget");
        int x = j.getHero().getX();
        int y = j.getHero().getY();
        Commande c =new Commande();
        c.droite = true;
        c.gauche = false;
        c.bas = false;
        c.haut = false;
        j.getHero().deplacer(c);
        assertEquals("mauvaise coordonnees X",x+1,j.getHero().getX());
        assertEquals("mauvaise coordonnees Y",y,j.getHero().getY());
    }

    @Test
    public void testEtreDefaiteVictoire(){
        Game j = new Game("amark");
        assertEquals("le jeu ni defaite ni victoire", j.etreDefaite(), false);
        assertEquals("le jeu ni defaite ni victoire", j.etreVictoire(), false);

        j.getHero().subirDegat(100);
        assertEquals("doit etre defaite", j.etreDefaite(), true);
        assertEquals("doit etre defaite", j.etreVictoire(), false);

        Game k = new Game("conan");
        k.getLm().clear();
        assertEquals("doit etre defaite", k.etreDefaite(), false);
        assertEquals("doit etre defaite", k.etreVictoire(), true);
    }

}