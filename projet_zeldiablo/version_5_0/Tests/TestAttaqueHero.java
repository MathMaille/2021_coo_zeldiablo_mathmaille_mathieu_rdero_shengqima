package version_5_0.Tests;

import org.junit.Test;
import version_5_0.JeuPackage.Game;
import version_5_0.JeuPackage.Monstre;
import version_5_0.moteurJeu.Commande;

import static org.junit.Assert.assertEquals;

public class TestAttaqueHero {
    /**
     * Le hero attaque et tue
     */
    @Test
    public void testAttaquePortee() {
        Game j = new Game("TIMI");
        j.getHero().setSens("droite");
        j.getHero().setPlace(3,9);
        j.getMonstre(0).setPlace(4,9);
        Commande c = new Commande();
        c.action=true;
        j.attaquerPerso(j.getHero(),c);
        assertEquals("il devrait  avoir 3 monstre",3,j.getNbMonstre());
    }

    /**
     * Le hero attaque un monstre hors portÃ©e
     */
    @Test
    public void testAttaqueHorsPortee() {
        Game j = new Game("TIMI");
        j.getHero().setSens("droite");
        j.getHero().setPlace(3,9);
        j.getMonstre(0).setPlace(5,9);
        Commande c = new Commande();
        c.action=true;
        j.attaquerPerso(j.getHero(),c);
        assertEquals("il devrait  avoir 4 monstre",4,j.getNbMonstre());
    }
}