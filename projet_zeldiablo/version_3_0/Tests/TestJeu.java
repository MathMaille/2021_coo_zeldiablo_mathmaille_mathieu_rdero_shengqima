package version_3_0.Tests;

import static org.junit.Assert.*;
import org.junit.Test;
import version_3_0.JeuPackage.Game;
import version_3_0.moteurJeu.Commande;

public class TestJeu {

    @Test
    public void testInitialiserMonstre() {
        Game j = new Game("Groget");
        assertEquals("Il doit y avoir 5 monstres",5,j.getNbMonstre());
    }
    @Test
    public void testdeplacer(){
        Game j = new Game("Groget");
        int x = j.getHero().getX();
        int y = j.getHero().getY();
        Commande c =new Commande();
        c.droite = true;
        c.gauche = false;
        c.bas = false;
        c.haut = false;
        j.getHero().deplacer(c);
        assertEquals("mauvaise coordonnees X",x+1,j.getHero().getX());
        assertEquals("mauvaise coordonnees Y",y,j.getHero().getY());
    }
}


