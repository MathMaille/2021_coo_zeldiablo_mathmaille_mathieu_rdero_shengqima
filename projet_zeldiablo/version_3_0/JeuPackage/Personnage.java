package version_3_0.JeuPackage;

public abstract class Personnage{

  /**
   * Attributs du Personnage
   */
  //vie du personnage
  private int vie;
  //dégats du personnage
  private int degat;
  //portée d'attaque du personnage
  private int portee;
  //position du personnage en x
  private int x;
  //position du personnage en y
  private int y;
  //le sens du personnage
  private String sens;

  private boolean mort;

  /**
   *
   * Constructeur de Personnage
   *
   * @param pv   int   points de vie du personnage
   * @param dgt   int   dégats du personnage
   * @param port  int  portée d'attaque du personnage
   */
  public Personnage(int pv,int dgt, int port){
    if(pv > 0 && dgt > 0 && port > 0){
      this.vie=pv;
      this.degat=dgt;
      this.portee=port;
      this.mort = false;
      this.sens="haut";
    }
  }

  public void subirDegat(int degat){

    if(degat >= vie)
      vie -= degat;
    else
      vie = 0;
    if(vie <= 0)
      mort = true;
  }

  /**
   * Getter vie
   *
   * @return int   vie du personnage
   */
  public int getVie(){
    return this.vie;
  }

  /**
   * Getter degat
   *
   * @return int  degats du personnage
   */
  public int getDegat(){
    return this.degat;
  }

  /**
   * Getter Portee
   *
   * @return int   portée du personnage
   */
  public int getPortee(){
    return this.portee;
  }

  /**
   * get x
   * @return int
   */
  public int getX() {
    return this.x;
  }

  /**
   * get y
   * @return int
   */
  public int getY() {
    return this.y;
  }

  public void setX(int x) {
    this.x = x;
  }

  public void setY(int y) {
    this.y = y;
  }

  public void setPlace(int j, int k){
    setY(k);
    setX(j);
  }

  public String getSens(){
    return this.sens;
  }

  public void setSens(String s){
    this.sens = s;
  }

  public boolean getMort(){
    return this.mort;
  }
}
