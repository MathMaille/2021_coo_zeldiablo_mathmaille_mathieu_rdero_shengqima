package version_3_0.JeuPackage;

import version_3_0.moteurJeu.*;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

public class DessinGame implements DessinJeu {

    private Game modele;
    private final int tailleCase=50;

    public DessinGame(Game g){
        this.modele = g;

    }

    @Override
    public void dessiner(BufferedImage image) {
        Graphics2D crayon = (Graphics2D) image.getGraphics();
        for(int i =0; i < modele.getPlateau().getTailleX(); i++){
            for(int j = 0; j< modele.getPlateau().getTailleY();j++){
                if(modele.getPlateau().getCase(i,j).etreMur()){
                    crayon.setColor(Color.black);
                } else {
                    crayon.setColor(Color.white);
                }
                crayon.fillRect(i*tailleCase,j*tailleCase, tailleCase, tailleCase);
                if(modele.getPlateau().getCase(i,j).etreOccupe()){
                    if (
                    modele.getPlateau().getCase(i, j).getPersonnage() instanceof Hero){
                        crayon.setColor(Color.blue);
                        crayon.fillOval(i*tailleCase,j*tailleCase, tailleCase, tailleCase);
                    }
                    if (modele.getPlateau().getCase(i, j).getPersonnage() instanceof Monstre){
                        crayon.setColor(Color.GRAY);
                        crayon.fillOval(i*tailleCase,j*tailleCase, tailleCase, tailleCase);
                    }
                }
            }
        }

    }
}
