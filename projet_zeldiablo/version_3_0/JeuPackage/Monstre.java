package version_3_0.JeuPackage;

public class Monstre extends Personnage {

      /**
       * Constructeur vide de Monstre
       */
      public Monstre(){
        super(2,1,1);
      }

      /**
       * Constructeur initialisent tous les attributs de Monstre
       * @param pv int : le nombre de point de vie du Hero
       * @param dgt int : les degat qu'inflige le Hero
       * @param port int : la portée d'attaque du Hero
       */
      public Monstre(int pv, int dgt, int port){
        super(pv,dgt,port);
      }

}
