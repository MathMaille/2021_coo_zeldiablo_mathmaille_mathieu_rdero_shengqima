package version_3_0.JeuPackage;

import version_3_0.moteurJeu.Commande;

public class Hero extends Personnage {

  /**
   * le nom du Hero
   */
  private final String nom;


  /**
   * Constructeur vide de Hero
   */
  public Hero(){
    super(5,2,1);
    this.nom="Timmie";
  }

  /**
   * Constructeur ne prenant uniquement qu'un String (le nom)
   * @param n String : le nom du hero
   */
  public Hero(String n){
    super(5,2,1);
    if(!n.equals("")){
      this.nom=n;
    } else {
      this.nom="Timmie";
    }
  }

  /**
   * Constructeur initialisent tous les attributs de Hero
   * @param pv int : le nombre de point de vie du Hero
   * @param dgt int : les degat qu'inflige le Hero
   * @param port int : la portée d'attaque du Hero
   * @param n String : le nom du hero
   */
  public Hero(int pv, int dgt, int port, String n){
    super(pv,dgt,port);
    if(!n.equals("")){
      this.nom=n;
    } else {
      this.nom="Timmie";
    }
  }

  public void deplacer(Commande commandeUser){

    if(commandeUser.bas){
      this.setSens("bas");
      this.setY(getY()+1);
    }
    if(commandeUser.droite){
      this.setSens("droite");
      this.setX(getX()+1);
    }
    if(commandeUser.haut){
      this.setSens("haut");
      this.setY(getY()-1);
    }
    if(commandeUser.gauche){
      this.setSens("gauche");
      this.setX(getX()-1);
    }
  }


  /**
   * @return String getNom retourne le nom du héro
   */
  public String getNom(){
    return this.nom;
  }




}
