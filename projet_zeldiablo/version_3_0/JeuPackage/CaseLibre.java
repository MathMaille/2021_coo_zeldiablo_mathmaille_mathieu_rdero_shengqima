package version_3_0.JeuPackage;

public class CaseLibre implements Case {

    Personnage occupe;

    /**
     * Constructeur vide de CaseLibre
     */
    public CaseLibre(){ }

    /**
     *
     * @return boolean   false car ce n'est pas un mur
     */
    @Override
    public boolean etreMur() {
        return false;
    }

    /**
     * Placer une personnage pour un caseLibre
     */
    public void setPersonnage(Personnage p) {

        if( !etreOccupe() )
            this.occupe = p;
    }
    /**
     * si un case possède une personnage
     */
    public boolean etreOccupe() {
        return this.occupe != null;
    }
    /**
     * supprimer une personnage pour un case
     */
    public void removePersonnage() {
        if(etreOccupe()) {
            this.occupe = null;
        }
    }
    /**
     *
     * @return personnage occupe
     */
    public Personnage getPersonnage() {
        return this.occupe;
    }
}
