package version_3_0.JeuPackage;

public interface Case {
    /**
     * Méthode qui permet de déterminer si la case est un mur
     *
     * @return boolean  true si la case est un mur
     */
    boolean etreMur();

    boolean etreOccupe();

    void setPersonnage(Personnage p);

    void removePersonnage();
    /**
     *
     * @return personnage occupe
     */
    Personnage getPersonnage();
}
