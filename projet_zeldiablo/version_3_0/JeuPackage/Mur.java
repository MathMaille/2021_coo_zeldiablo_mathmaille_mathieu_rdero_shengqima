package version_3_0.JeuPackage;

public class Mur implements Case {

    /**
     * Constructeur de Mur
     */
    public Mur() { }

    /**
     *
     * @return boolean   true car c'est un mur
     */
    @Override
    public boolean etreMur() {
        return true;
    }

    @Override
    public boolean etreOccupe() {
        return false;
    }

    @Override
    public void setPersonnage(Personnage p) {

    }

    @Override
    public void removePersonnage() {

    }

    @Override
    public Personnage getPersonnage() {
        return null;
    }
}
