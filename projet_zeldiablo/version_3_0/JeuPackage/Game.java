package version_3_0.JeuPackage;

import version_3_0.moteurJeu.*;

import java.awt.event.KeyListener;
import java.util.ArrayList;

public class Game implements Jeu{

    private Plateau p;
    private Hero moi;
    private ArrayList<Monstre> lm;

    public Game(String nom){
        initiliserPlateau();
        initialisationHero("Tim y");
        initialisationMonstres(5);
    }

    public void initiliserPlateau(){
        this.p = new Plateau();
        for(int i=0;i<10;i++){
            boolean trouver = false;
            while(!trouver){
                int k = (int) (Math.random() * (10));
                int j = (int) (Math.random() * (10));
                if(!this.p.getCase(j,k).etreMur()){
                    this.p.placerMur(j,k);
                    trouver = true;
                }
            }
        }

    }

    public void initialisationHero(String n){
        this.moi = new Hero(n);
        boolean trouver = false;
        while(!trouver){
            int k = (int) Math.round(Math.random()*(9));
            int j = (int) Math.round(Math.random()*(9));
            if(!this.p.getCase(j,k).etreMur()  && !this.p.getCase(j,k).etreOccupe()){
                this.moi.setPlace(j,k);
                this.p.setPersonnage(moi,j,k);
                trouver = true;
            }
        }

    }

    public void initialisationMonstres(int f){
        lm = new ArrayList<Monstre>();
        for(int i=0;i<f;i++){
            this.lm.add(new Monstre());
            boolean trouver = false;
            while(!trouver){
                int k = (int) Math.round(Math.random()*(9));
                int j = (int) Math.round(Math.random()*(9));
                if(!this.p.getCase(j,k).etreMur()  && !this.p.getCase(j,k).etreOccupe()){
                    this.lm.get(i).setPlace(j,k);
                    this.p.setPersonnage(lm.get(i),j,k);
                    trouver = true;
                }
            }
        }
    }



    /**
     * Methodes Getters
     */
    public Plateau getPlateau(){
        return this.p;
    }
    public Hero getHero(){
        return this.moi;
    }
    public Monstre getMonstre(int index){
        return this.lm.get(index);
    }
    public int getNbMonstre(){
        return this.lm.size();
    }


    public void deplacerHero(Commande commandeUser){
        if(commandeUser.bas && !commandeUser.haut && !commandeUser.gauche && !commandeUser.droite && moi.getY()<9){
            if(!getPlateau().getCase(moi.getX(),moi.getY()+1).etreMur() &&
                    !getPlateau().getCase(moi.getX(),moi.getY()+1).etreOccupe()){
                moi.deplacer(commandeUser);
            }
        }

        else if(commandeUser.droite && !commandeUser.bas && !commandeUser.haut && !commandeUser.gauche && moi.getX()<9){
            if(!getPlateau().getCase(moi.getX()+1,moi.getY()).etreMur() &&
                    !getPlateau().getCase(moi.getX()+1,moi.getY()).etreOccupe()){
                moi.deplacer(commandeUser);
            }
        }

        else if(commandeUser.haut && !commandeUser.bas && !commandeUser.gauche && !commandeUser.droite && moi.getY()>0){
            if(!getPlateau().getCase(moi.getX(),moi.getY()-1).etreMur() &&
                    !getPlateau().getCase(moi.getX(),moi.getY()-1).etreOccupe()){
                moi.deplacer(commandeUser);
            }
        }

        else if(commandeUser.gauche && !commandeUser.bas && !commandeUser.haut && !commandeUser.droite && moi.getX()>0){
            if(!getPlateau().getCase(moi.getX()-1,moi.getY()).etreMur() &&
                    !getPlateau().getCase(moi.getX()-1,moi.getY()).etreOccupe()){
                moi.deplacer(commandeUser);
            }
        }
    }

    @Override
    public void evoluer(Commande commandeUser) {

        this.p.getCase(moi.getX(),moi.getY()).removePersonnage();
        deplacerHero(commandeUser);
        this.p.getCase(moi.getX(), moi.getY()).setPersonnage(moi);
    }

    @Override
    public boolean etreFini() {
        return false;
    }



}
