public class Mur implements Case{

    /**
     * Constructeur de Mur
     */
    public Mur() { }

    /**
     *
     * @return boolean   true car c'est un mur
     */
    @Override
    public boolean etreMur() {
        return true;
    }
}
