public class Plateau {
    /**
     * taille en abscisse du Plateau
     */
    private int x;
    /**
     * taille en ordonnées du Plateau
     */
    private int y;
    /**
     * Tableau de case
     */
    private Case [][] tab;

    /**
     * Constructeur du Plateau
     *
     * @param ix int  taille en abscisse du Plateau
     * @param iGrec  int taille en ordonnées du Plateau
     */
    public Plateau(int ix, int iGrec){
        this.x = ix;
        this.y = iGrec;
        this.tab= new Case[ix][iGrec];
        for (int i = 0; i<ix; i++){
            for (int j = 0; j<iGrec; j++){
                this.tab[i][j] = new CaseLibre();
            }
        }
    }

    /**
     * Constructeur vide du Plateau
     */
    public Plateau(){
        this.x = 10;
        this.y = 10;
        this.tab= new Case[10][10];
        for (int i = 0; i<10; i++){
            for (int j = 0; j<10; j++){
                this.tab[i][j] = new CaseLibre();
            }
        }
    }

    /**
     * Getter x
     *
     * @return int Taille du tableau en abscisse
     */
    public int getTailleX(){
        return this.x;
    }

    /**
     * Getter y
     *
     * @return int Taille du tableau en ordonnée
     */
    public int getTailleY(){
        return this.y;
    }

    /**
     *  Getter Case
     *
     * @param ix int  abscisse de la case
     * @param iGrec int  ordonnée de la case
     * @return Case  case du plateau aux coordonnées souhaitées
     */
    public Case getCase(int ix, int iGrec){
        return this.tab[ix][iGrec];
    }

    /**
     * Méthode qui permet de placer un mur
     *
     * @param ix int  abscisse du mur
     * @param iGrec int  ordonnée du mur
     */
    public void placerMur(int ix, int iGrec){
        this.tab[ix][iGrec] = new Mur();
    }
}
