public class Jeu {
    /**
     * Plateau de jeu
     */
    private Plateau p;

    /**
     * Constructeur vide de Jeu
     */
    public Jeu() { }

    /**
     * Méthode qui initialise un pLateau dans le jeu
     */
    public void initialiserPlateau(){
        this.p = new Plateau();
    }

    /**
     *
     * @return Plateau  retourne le plateau de jeu
     */
    public Plateau getPlateau(){
        return this.p;
    }
}
