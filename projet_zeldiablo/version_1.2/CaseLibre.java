public class CaseLibre implements Case{

    /**
     * Constructeur vide de CaseLibre
     */
    public CaseLibre(){ }

    /**
     *
     * @return boolean   false car ce n'est pas un mur
     */
    @Override
    public boolean etreMur() {
        return false;
    }
}
