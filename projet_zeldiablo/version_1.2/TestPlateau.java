import static org.junit.Assert.*;
import org.junit.Test;

/**
 * classe qui permet de tester le plateau
 */
public class TestPlateau {
    /**
     * verifie la taille x du tableau
     */
    @Test
    public void testTailleXJeu() {
        Jeu j=new Jeu();
        j.initialiserPlateau();
        assertEquals("il devrait y avoir une taille de 10",10,j.getPlateau().getTailleX());
    }

    /**
     * verifie la taille y du tableau
     */
    @Test
    public void testTailleYJeu() {
        Jeu j=new Jeu();
        j.initialiserPlateau();
        assertEquals("il devrait y avoir une taille de 10",10,j.getPlateau().getTailleY());
    }

    /**
     * verifie si la case est un mur
     */
    @Test
    public void testMur() {
        Jeu j=new Jeu();
        j.initialiserPlateau();
        assertEquals("il devrait y avoir une case vide donc false",false,j.getPlateau().getCase(5,5).etreMur());
    }

    /**
     * place un mur et verifie si la case est un mur
     */
    @Test
    public void testSetMur() {
        Jeu j=new Jeu();
        j.initialiserPlateau();
        j.getPlateau().placerMur(5, 5);
        assertEquals("il devrait y avoir un un mur donc true",true,j.getPlateau().getCase(5,5).etreMur());
    }
}
