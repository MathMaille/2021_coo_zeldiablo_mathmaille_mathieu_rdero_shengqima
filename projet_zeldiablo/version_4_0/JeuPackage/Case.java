package version_5_0.JeuPackage;

public interface Case {
    /**
     * Methode qui permet de determiner si la case est un murs
     * @return boolean  true si la case est un mur
     */
    boolean etreMur();

    /**
     * Determine si la case possede un personnage
     * @return boolean true si la case possede un personnage
     */
    boolean etreOccupe();

    /**
     * Mets un personnage sur la case
     * @param p le personnage
     */
    void setPersonnage(Personnage p);

    /**
     * Retire le personnage de la case
     */
    void removePersonnage();

    /**
     * @return personnage qui occupe la case
     */
    Personnage getPersonnage();
    /**
     *
     * @param t  Piege à placer sur la case
     */
    void setTrap(Piege t);

    /**
     * enlève le piège de la case
     */
    void removeTrap();

    /**
     *
     * @return boolean  true si c'est piegé
     */
    boolean etrePiege();

    /**
     *
     * @return Piege   le piege de la case
     */
    Piege getTrap();
}
