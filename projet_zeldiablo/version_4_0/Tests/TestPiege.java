package version_4_0.Tests;

import static org.junit.Assert.*;
import org.junit.Test;
import version_4_0.JeuPackage.Game;
import version_4_0.JeuPackage.Hero;
import version_4_0.JeuPackage.Piege;

public class TestPiege {
    /**
     * verifie que le piège blesse
     */
    @Test
    public void testBlesse() {
        Hero h = new Hero("Sam");
        Piege p = new Piege();
        p.Blesser(h);
        assertEquals("il avoir 4 pv",4,h.getVie());
    }

    /**
     * verifie que le piège disparait après avoir fonctionné
     */
    @Test
    public void testSupp() {
        Game j=new Game("frodons");
        j.getHero().setPlace(3,4);
        j.prendrePiege(j.getHero());
        assertEquals("il avoir 4 pv",4,j.getHero().getVie());
        assertEquals("il avoir 4 pv",false,j.getPlateau().getCase(3,4).etrePiege());
    }

    /**
     * verifie que le piège tue
     */
    @Test
    public void testTue() {
        Hero h = new Hero("Sam");
        Piege p = new Piege();
        h.subirDegat(4);
        p.Blesser(h);
        assertEquals("il devrait être mort",true,h.getMort());
    }
}
