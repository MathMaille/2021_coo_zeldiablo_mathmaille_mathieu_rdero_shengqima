package version_4_0.Tests;

import static org.junit.Assert.*;
import org.junit.Test;
import version_4_0.JeuPackage.Game;

/**
 * classe qui permet de tester le plateau
 */
public class TestPlateau {
    /**
     * verifie la taille du tableau
     */
    @Test
    public void testTailleJeu() {
        Game j=new Game("frodons");
        assertEquals("il devrait x avoir une taille de 10",10,j.getPlateau().getTailleX());
        assertEquals("il devrait y avoir une taille de 10",10,j.getPlateau().getTailleY());
    }

    /**
     * verifie les cases du tableau
     */
    @Test
    public void testCasesJeu() {
        Game j=new Game("frodons");
        //case en bas à droite (mur)
        assertEquals("il devrait y avoir un mur",true,j.getPlateau().getCase(9,9).etreMur());
        //case libre
        assertEquals("il devrait y avoir une case libre",false,j.getPlateau().getCase(8,8).etreMur());
        //case libre avec personnage
        assertEquals("il devrait y avoir une case occupée",true,j.getPlateau().getCase(5,9).etreOccupe());
        //case piégée
        assertEquals("il devrait y avoir une case piégée",true,j.getPlateau().getCase(3,4).etrePiege());
    }


}